# WeatherAppEficode

The application demonstrates the use of weather data which is downloaded from the proxy server. 'WeatherAppEficode uses mvc design pattern with third party frameworks Alamofire, 
SnapKit and PromiseKit.
The app uses free background image.
https://pngtree.com/freebackground/weather-forecast-creative-background_436779.html


## Requirements

xCode 9.2 with Simulator and MacOS Sierra


## Using the application

### How to run

1. Terminal: Navigate to root of the project and command pod install
2. Finder: Open automatically created workspace file on project root
3. xCode: Run the project


### Main

MainView

This is main view of view controller. It is separated from ViewController implementation. 
It handles location manager and connection manager callbacks. All view setups are straight implemented to the source file.


WeatherView

Custom view to show effects and handle loaded image.

