//
//  ConnectionManager.swift
//  WeatherAppEficode
//
//  Created by Antti Oinaala on 04/12/2017.
//  Copyright © 2017 Antti Oinaala Work. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

final class ConnectionManager: NSObject {
    static let shared = ConnectionManager()
    
    fileprivate let baseURLPath = "weatherapp.eficode.fi"
    fileprivate let imageBaseURLString = "https://weatherapp.eficode.fi/img"
    
    func loadWeather(lat: Double?, lon: Double?) -> Promise<WeatherItem> {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = baseURLPath
        urlComponents.path = "/api/forecast"
        
        if let latitude = lat, let longitude = lon {
            let latQueryItem = URLQueryItem(name: "lat", value: "\(latitude)")
            let lonQueryItem = URLQueryItem(name: "lon", value: "\(longitude)")
            urlComponents.queryItems = [latQueryItem, lonQueryItem]
        }
        
        return Promise { fulfill, reject in
            if let urlString = urlComponents.string {
                Alamofire.request(urlString, method: .get, parameters: nil, encoding:JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        if let dict = response.result.value as? [String: Any] {
                            let weatherItem = self.parseWeatherItem(data: dict)
                            fulfill(weatherItem)
                        }
                    case .failure(let error):
                        reject(error)
                    }
                })
            } else {
                reject(NSError(domain: "0", code: -1, userInfo: ["Error": "Bad URL"]))
            }
        }
    }
    
    func loadIconURL(name: String) -> URL? {
        let baseURL = URL(string: imageBaseURLString)
        var finalURL = baseURL?.appendingPathComponent(name)
        finalURL = finalURL?.appendingPathExtension("svg")
        
        if let finalURL = finalURL {
            return finalURL
        } else {
            return nil
        }
    }
}

// MARK - Parser

extension ConnectionManager {
    fileprivate func parseWeatherItem(data: [String: Any]) -> WeatherItem {
        let item = WeatherItem()
        if let icon = data["icon"] as? String {
            item.iconName = icon
        }
        
        return item
    }
}
