//
//  ViewController.swift
//  WeatherAppEficode
//
//  Created by Antti Oinaala on 04/12/2017.
//  Copyright © 2017 Antti Oinaala Work. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func loadView() {
        let mainView = MainView()
        mainView.delegate = self
        view = mainView
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        return UIInterfaceOrientation.portrait
    }
}

// MARK - Alert delegate functions

extension ViewController: AlertViewDelegate {
    func showAuthorizationStatusAlert() {
        // alert that location services not authorized by the user
        let alert = UIAlertController(
            title: "Location service not authorized",
            message: "Go to settings and give the app permission to use location services",
            preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("action.ok", tableName: "Localizable", value: "Ok", comment: "Ok action"), style: .cancel, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
}

