//
//  LocationManager.swift
//  WeatherAppEficode
//
//  Created by Antti Oinaala on 07/12/2017.
//  Copyright © 2017 Antti Oinaala Work. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegate: class {
    func gotNewLocation(lat: Double, lon: Double)
    func didChangeAuthorizationStatus()
}

final class LocationManager: NSObject, CLLocationManagerDelegate {
    static let shared = LocationManager()
    fileprivate let locationManager = CLLocationManager()
    
    weak var delegate: LocationManagerDelegate?
    
    fileprivate override init() {
        super.init()
        
        locationManager.delegate = self
    }
}

// MARK - Authorization

extension LocationManager {
    internal var isAuthorized: Bool {
        let status = CLLocationManager.authorizationStatus()
        return status == CLAuthorizationStatus.authorizedWhenInUse
    }

    internal func requestAuthorization() {
        if !isAuthorized {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    internal func requestLocation() {
        locationManager.requestLocation()
    }
}

// MARK - Location delegate methods

extension LocationManager {
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if delegate != nil, let latitude = locations.first?.coordinate.latitude, let longitude = locations.first?.coordinate.longitude  {
            delegate?.gotNewLocation(lat: latitude, lon: longitude)
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    internal func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        delegate?.didChangeAuthorizationStatus()
    }
}
