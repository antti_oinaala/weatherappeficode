//
//  WeatherView.swift
//  WeatherAppEficode
//
//  Created by Antti Oinaala on 14/12/2017.
//  Copyright © 2017 Antti Oinaala Work. All rights reserved.
//

import UIKit
import WebKit

protocol WeatherViewDelegate: class {
    func didTapGettingNewLocation()
}

final class WeatherView: UIView {
    fileprivate var didSetupConstraints = false
    fileprivate var didSetupLayers = false
    
    fileprivate var webView = WKWebView()
    fileprivate var gestureView = UIView()
    fileprivate var loadingCover = UIView()
    fileprivate var indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    weak var delegate: WeatherViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        defer {
            didSetupConstraints = true
            super.updateConstraints()
        }
        
        guard didSetupConstraints == false else { return }
        
        webView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
        
        gestureView.snp.makeConstraints { make in
            make.edges.equalTo(webView)
        }
        
        loadingCover.snp.makeConstraints { make in
            make.edges.equalTo(gestureView)
        }
        
        indicator.snp.makeConstraints { make in
            make.center.equalTo(loadingCover)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard didSetupLayers == false else { return }
        didSetupLayers = true
        
        let borderLayer = CALayer()
        let borderWidth = 10
        var frame = gestureView.bounds
        frame = CGRect(x: frame.origin.x-CGFloat(borderWidth), y: frame.origin.y-CGFloat(borderWidth), width: frame.size.width+CGFloat(borderWidth*2), height: frame.size.height+CGFloat(borderWidth*2))
        borderLayer.frame = frame
        borderLayer.borderColor = UIColor(red: 0/255, green: 163/255, blue:228/255, alpha: 1).cgColor
        borderLayer.borderWidth = CGFloat(borderWidth)
        borderLayer.cornerRadius = 6
        borderLayer.shadowOffset = CGSize(width: 0, height: 6)
        borderLayer.shadowRadius = 6
        borderLayer.shadowColor = UIColor.black.cgColor
        borderLayer.shadowOpacity = 1
        gestureView.layer.addSublayer(borderLayer)
    }
}

// MARK: - Private methods

fileprivate extension WeatherView {
    func setup() {
        setupViews()
        setupUX()
    }
    
    func setupViews() {
        addSubview(webView)
        addSubview(gestureView)
        addSubview(loadingCover)
        loadingCover.addSubview(indicator)
    }
    
    func setupUX() {
        webView.scrollView.isScrollEnabled = false
        webView.contentMode = .scaleAspectFit
        webView.navigationDelegate = self
        webView.isOpaque = false
        webView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(WeatherView.onWeatherCheck(_:)))
        gestureView.addGestureRecognizer(tapGesture)
        gestureView.isUserInteractionEnabled = true
        
        loadingCover.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.8)
        indicator.startAnimating()
        showLoadingCover()
    }
}

// MARK: - Button callback

fileprivate extension WeatherView {
    @objc func onWeatherCheck(_ sender: UITapGestureRecognizer) {
        showLoadingCover()
        
        if let delegate = delegate {
            delegate.didTapGettingNewLocation()
        }
    }
}

// MARK: - Public methods

extension WeatherView {
    func loadWeatherImage(url: URL) {
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
    func showLoadingCover() {
        loadingCover.isHidden = false
    }
    
    func hideLoadingCover() {
        loadingCover.isHidden = true
    }
}

// MARK: - Delegate methods

extension WeatherView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideLoadingCover()
    }
}
