//
//  MainView.swift
//  WeatherAppEficode
//
//  Created by Antti Oinaala on 04/12/2017.
//  Copyright © 2017 Antti Oinaala Work. All rights reserved.
//

import UIKit
import SnapKit
import WebKit

protocol AlertViewDelegate: class {
    func showAuthorizationStatusAlert()
}

class MainView: UIView, LocationManagerDelegate, WeatherViewDelegate {
    fileprivate var didSetupConstraints = false
    fileprivate var weatherView = WeatherView()
    fileprivate var backgroundImageView = UIImageView()
    fileprivate var titleTextField = UITextField()
    fileprivate var effectView = UIVisualEffectView()
    
    weak var delegate: AlertViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        
        weatherView.delegate = self
        LocationManager.shared.delegate = self
        
        requestLocation()
        
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        defer {
            didSetupConstraints = true
            super.updateConstraints()
        }
        
        guard didSetupConstraints == false else { return }
        
        backgroundImageView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
        
        effectView.snp.makeConstraints { make in
            make.edges.equalTo(backgroundImageView)
        }
        
        titleTextField.snp.makeConstraints { make in
            make.top.equalTo(self).inset(40)
            make.left.equalTo(self).inset(20)
            make.right.equalTo(self).inset(20)
        }
        
        weatherView.snp.makeConstraints { make in
            make.centerY.equalTo(self)
            make.centerX.equalTo(self)
            make.left.equalTo(self).inset(50)
            make.right.equalTo(self).inset(50)
            make.height.equalTo(weatherView.snp.width)
        }
    }
}

// MARK: - Private Functions

fileprivate extension MainView {
    func setup() {
        setupViews()
        setupVisuals()
    }
    
    func setupViews() {
        addSubview(backgroundImageView)
        addSubview(effectView)
        addSubview(titleTextField)
        addSubview(weatherView)
    }
    
    func setupVisuals() {
        let font = UIFont(name: "HelveticaNeue-Bold", size: 51)
        titleTextField.font = font
        titleTextField.textAlignment = .center
        titleTextField.textColor = UIColor(red: 0/255, green: 163/255, blue:228/255, alpha: 1)
        titleTextField.text = "Weather App"
        
        let image = #imageLiteral(resourceName: "LaunchImage")
        backgroundImageView.image = image
        
        let blurEffect = UIBlurEffect(style: .dark)
        effectView.effect = blurEffect
        effectView.alpha = 0
        
        UIView.animate(withDuration: 0.8, animations: {
            self.effectView.alpha = 0.4
        })
    }
    
    func loadWeather(lat: Double, lon: Double) {
    //    let lat = 20.932933
    //    let lon = -76.103516
        
        ConnectionManager.shared.loadWeather(lat: lat, lon: lon).then { item -> Void in
            if let icon = item.iconName {
                if let svgURL = ConnectionManager.shared.loadIconURL(name: icon) {
                    self.weatherView.loadWeatherImage(url: svgURL)
                }
            }
            }.catch { error in
                print("Error: \(error.localizedDescription)")
        }
    }
    
    func requestLocation() {
        if LocationManager.shared.isAuthorized {
            LocationManager.shared.requestLocation()
        } else {
            // request authorization from user
            LocationManager.shared.requestAuthorization()
        }
    }
}

// MARK: - Delegate methods

extension MainView {
    func gotNewLocation(lat: Double, lon: Double) {
        loadWeather(lat: lat, lon: lon)
    }
    
    internal func didChangeAuthorizationStatus() {
        if LocationManager.shared.isAuthorized {
            LocationManager.shared.requestLocation()
        } else {
            if delegate != nil {
                delegate?.showAuthorizationStatusAlert()
            }
            
            weatherView.hideLoadingCover()
        }
    }
    
    func didTapGettingNewLocation() {
        requestLocation()
    }
}
